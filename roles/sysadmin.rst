=====================
System Administration
=====================

Website
=======

The PyCon ZA website is hosted on the `CTPUG <https://www.ctpug.org.za>`_ server,
with the wildcard domain entry provided by the FSF.

The website is powered by a `wafer <https://github.com/CTPUG/wafer>`_ instance.

The sysadmin team is responsible for maintaining the infrastructure needed for
the website, which includes updating and maintaining the wafer installation
and deploying any site updates that require access to the system.

Organiser Repo
==============

For sharing information amongst the committee, we host per-year private git
repos on the `CTPUG <https://www.ctpug.org.za>`_ server.

This repo is administered using
`gitolite <http://gitolite.com/gitolite/index.html>`_.
The sysadmin team is responsible for setting up the repo, managing the access
keys to the repository and managing any notification settings (irker, slack, etc)
required.

PyCon ZA theme repo
===================

We create and run a repo for the theming and additional features needed for
PyCon ZA. This is created under the CTPUG organisation on
`github <https://github.com/CTPUG/>`_.
